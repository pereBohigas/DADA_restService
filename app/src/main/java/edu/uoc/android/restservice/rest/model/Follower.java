package edu.uoc.android.restservice.rest.model;

import com.google.gson.annotations.SerializedName;

public class Follower {

    @SerializedName("login")
    private String login;

    @SerializedName("id")
    private Integer id;

    @SerializedName("avatar_url")
    private String avatarUrl;

    @SerializedName("type")
    private String type;

    public String getLogin() {
        return login;
    }

    public Integer getId() {
        return id;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getType() {
        return type;
    }
}
