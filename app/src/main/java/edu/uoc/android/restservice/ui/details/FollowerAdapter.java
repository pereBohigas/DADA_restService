package edu.uoc.android.restservice.ui.details;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.model.Follower;

public class FollowerAdapter extends RecyclerView.Adapter<FollowerAdapter.ViewHolder> {

    private Context context;
    private List<Follower> followerList;

    public FollowerAdapter(List<Follower> followersList) {
        this.followerList = followersList;
    }

    @Override
    public FollowerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_follower, viewGroup, false);
        context = viewGroup.getContext();
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }
    
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.followerName.setText(followerList.get(position).getLogin());
        Picasso.get().load(followerList.get(position).getAvatarUrl()).into(viewHolder.followerPhoto);
    }

    @Override
    public int getItemCount() {
        return followerList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView followerPhoto;
        TextView followerName;

        ViewHolder(View view) {
            super(view);
            followerPhoto = view.findViewById(R.id.followerPhoto);
            followerName = view.findViewById(R.id.followerName);
        }
    }

}
